/*******************
 *	   GB-KEY	   *
 * extend keyboard *
 *	  TeamKNOx	   *
 *	  07/2000	   *
 *******************/
 
#include <gb.h>
#include <stdlib.h>
#include "bkg.h"
#include "obj.h"

/* extern */
extern UBYTE key_raw_ir[8];

/* definition */
#define OFF		0
#define ON		1

/* character data */
#include "bkg.c"
#include "obj.c"
UWORD bkg_palette[] = {
	bkgCGBPal0c0, bkgCGBPal0c1, bkgCGBPal0c2, bkgCGBPal0c3,
};
UWORD obj_palette[] = {
	objCGBPal0c0, objCGBPal0c1, objCGBPal0c2, objCGBPal0c3,
};

/* message */
unsigned char msg_title[] = { "=== EXT.KEYBOARD ===" };
unsigned char msg_key[]	  = { "key value  :" };
unsigned char msg_ascii[] = { "ascii value:" };
unsigned char msg_hline[] = { "--------------------" };
unsigned char msg_clr[]	  = { "                    " };
unsigned char msg_knox[]  = { "TeamKNOx" };

/* keyboad data */
//#include "ibm_kb_table.c"
#include "109jp_kb_table.c"

/* work area */
UBYTE cursor_x;
UBYTE cursor_y;
UBYTE key_raw_ir_old;
UBYTE key_ascii_value;
UBYTE key_ascii_value_old;

/* functoins */
void init_character()
{
  set_bkg_data( 0, 128, bkg );
  set_sprite_data( 0, 1, obj );
  set_bkg_palette( 0, 1, bkg_palette );
  set_sprite_tile( 0, 0 );
  set_sprite_palette( 0, 1, obj_palette );
  set_sprite_prop( 0, 0 );
  SHOW_BKG;
  SHOW_SPRITES;
  DISPLAY_ON;
  enable_interrupts();
}

void cls( UBYTE y1, UBYTE y2 )
{
  UBYTE y;
  for ( y=y1; y<y2+1; y++ ) {
	set_bkg_tiles( 0, y, 20, 1, msg_clr );
  }
}

void disp_value( UWORD d, UWORD e, UBYTE c, UBYTE x, UBYTE y )
{
  UWORD m;
  UBYTE i, n;
  unsigned char data[6];

  m = 1;
  if ( c > 1 ) {
	for ( i=1; i<c; i++ ) {
	  m = m * e;
	}
  }
  for ( i=0; i<c; i++ ) {
	n = d / m; d = d % m; m = m / e;
	data[i] = '0' + n;		 /* '0' - '9' */
	if ( data[i] > '9' )  data[i] += 7;
  }
  set_bkg_tiles( x, y, c, 1, data );
}

void fwd_cursor()
{
	cursor_x++;
	if( cursor_x > 19) {
		cursor_x = 0;
		cursor_y++;
		if( cursor_y > 17) {
			cursor_y = 4;
		}
	}
	move_sprite( 0, cursor_x*8+8, cursor_y*8+16 );
}

void bwd_cursor()
{
	if( cursor_x == 0 ) {
		cursor_x = 19;
		cursor_y--;
		if( cursor_y < 4) {
			cursor_y = 17;
		}
	} else {
		cursor_x--;
	}
	move_sprite( 0, cursor_x*8+8, cursor_y*8+16 );
}

void convert_to_ascii()
{
	if( key_raw_ir[0] & 0x80 ) {	/* check for key up */
		key_ascii_value = 0xFF;
		delay(10);
		return;
	}
	key_ascii_value = key_table[key_raw_ir[0]];

	if( key_ascii_value_old == 0x0F ) {		/* convert to shift char */
		if( (key_ascii_value>0x3F) && (key_ascii_value<0x5F) ) {
			key_ascii_value = key_ascii_value + 0x20;
		} if( (key_ascii_value>0x30) && (key_ascii_value<0x3C) ) {
			key_ascii_value = key_ascii_value - 0x10;
		} if( (key_ascii_value>0x2B) && (key_ascii_value<0x30) ) {
			key_ascii_value = key_ascii_value + 0x10;
		}
	}
}

void write_to_screen()
{
	unsigned char data;
	
	disp_value( key_raw_ir[0], 16, 2, 14, 1 );
	disp_value( key_raw_ir[1], 16, 2, 17, 1 );
	disp_value( key_ascii_value, 16, 2, 14, 2 );

	if( key_raw_ir[0] == key_raw_ir_old ) {			/* key repeat enable */
		key_raw_ir_old = 0;
		return;
	}
	key_raw_ir_old = key_raw_ir[0];
	key_ascii_value_old = key_ascii_value;

	if( key_ascii_value & 0x80 ) return;			/* check for non print char */

	if( key_ascii_value > 0x1F ) {					/* ascii code 0x20 to 0x7F */
		data = key_ascii_value;
		set_bkg_tiles( cursor_x, cursor_y, 1, 1, &data );
		fwd_cursor();
	} else if( key_ascii_value == 0x08 ) {			/* backspace key */
		bwd_cursor();
		set_bkg_tiles( cursor_x, cursor_y, 1, 1, msg_clr );
	} else if( key_ascii_value == 0x0D ) {			/* enter key */
		cursor_x = 19;
		fwd_cursor();
	} else if( key_ascii_value == 0x01 ) {			/* test1 cls */
		cls( 4, 17 );
		cursor_x = 0;
		cursor_y = 4;
		move_sprite( 0, cursor_x*8+8, cursor_y*8+16 );
	} else if( key_ascii_value == 0x02 ) {			/* test2 TeamKNOx */
		cursor_x = 19;
		fwd_cursor();
		set_bkg_tiles( cursor_x, cursor_y, 8, 1, msg_knox );
		cursor_x = 8;
		move_sprite( 0, cursor_x*8+8, cursor_y*8+16 );
	}

//	  disp_value( cursor_x, 16, 2, 0, 16 );
//	  disp_value( cursor_y, 16, 2, 0, 17 );
}

void main()
{
	init_character();
	/* initialize bank number */
	ENABLE_RAM_MBC1;
	SWITCH_RAM_MBC1( 0 );
	cursor_x = 0;
	cursor_y = 4;
	cls( 0, 17 );
	move_sprite( 0, cursor_x*8+8, cursor_y*8+16 );
	set_bkg_tiles( 0, 0, 20, 1, msg_title );
	set_bkg_tiles( 1, 1, 12, 1, msg_key );
	set_bkg_tiles( 1, 2, 12, 1, msg_ascii );
	set_bkg_tiles( 0, 3, 20, 1, msg_hline );
	while( 1 ) {
		get_raw_signal_key();
		convert_to_ascii();
		write_to_screen();
	}
}

/* EOF */