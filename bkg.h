/*

 BKG.H

 Include File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 127

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v1.6

*/


/* Bank of tiles. */
#define bkgBank 0

/* Super Gameboy palette 0 */
#define bkgSGBPal0c0 8192
#define bkgSGBPal0c1 0
#define bkgSGBPal0c2 6108
#define bkgSGBPal0c3 8935

/* Super Gameboy palette 1 */
#define bkgSGBPal1c0 6596
#define bkgSGBPal1c1 6368
#define bkgSGBPal1c2 6108
#define bkgSGBPal1c3 8935

/* Super Gameboy palette 2 */
#define bkgSGBPal2c0 6596
#define bkgSGBPal2c1 6368
#define bkgSGBPal2c2 6108
#define bkgSGBPal2c3 8935

/* Super Gameboy palette 3 */
#define bkgSGBPal3c0 6596
#define bkgSGBPal3c1 6368
#define bkgSGBPal3c2 6108
#define bkgSGBPal3c3 8935

/* Gameboy Color palette 0 */
#define bkgCGBPal0c0 32767
#define bkgCGBPal0c1 24311
#define bkgCGBPal0c2 15855
#define bkgCGBPal0c3 0

/* Gameboy Color palette 1 */
#define bkgCGBPal1c0 32767
#define bkgCGBPal1c1 24311
#define bkgCGBPal1c2 15855
#define bkgCGBPal1c3 25368

/* Gameboy Color palette 2 */
#define bkgCGBPal2c0 32767
#define bkgCGBPal2c1 24311
#define bkgCGBPal2c2 15855
#define bkgCGBPal2c3 0

/* Gameboy Color palette 3 */
#define bkgCGBPal3c0 6108
#define bkgCGBPal3c1 8935
#define bkgCGBPal3c2 6596
#define bkgCGBPal3c3 6368

/* Gameboy Color palette 4 */
#define bkgCGBPal4c0 6108
#define bkgCGBPal4c1 8935
#define bkgCGBPal4c2 6596
#define bkgCGBPal4c3 6368

/* Gameboy Color palette 5 */
#define bkgCGBPal5c0 6108
#define bkgCGBPal5c1 8935
#define bkgCGBPal5c2 6596
#define bkgCGBPal5c3 6368

/* Gameboy Color palette 6 */
#define bkgCGBPal6c0 6108
#define bkgCGBPal6c1 8935
#define bkgCGBPal6c2 6596
#define bkgCGBPal6c3 6368

/* Gameboy Color palette 7 */
#define bkgCGBPal7c0 6108
#define bkgCGBPal7c1 8935
#define bkgCGBPal7c2 6596
#define bkgCGBPal7c3 6368
/* Start of tile array. */
extern unsigned char bkg[];

/* End of BKG.H */
