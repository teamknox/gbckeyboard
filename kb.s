;************************
;* IBM Keyboard Reader  *
;* Ken Kaarvik Mar30/99 *
;*----------------------*
;*   Ported for GBDK    *
;*  TeamKNOx Jun30/00   *
;************************
;Wiring info
;
;Connect your Color Gameboy to your keyboard
;
;GBC serial port   AT keyboard or PS/2 keyboard
;
;pin1 +5v          pin5 +5v       pin4 +5v
;pin2 Sout         nc             nc
;pin3 Sin          pin2 Data      pin1 Data
;pin4 (was P14)    pin1 Clk       pin5 Clock
;pin5 Clk          pin1 Clk       pin5 Clock
;pin6 0v           pin4 0v        pin3 GND
;
;Pins 4&5 of the GBC serial port are connected
;together and connect to pin1 of the keyboard.
;
;Note : Print Screen & Pause aren't decoded properly
;
;Add pin assign of PS/2 keyboard  by TeamKNOx

    .area   _BSS
_key_raw_ir::
    .DS 8

    .area   _CODE
_get_raw_signal_key::
    di
    ld  a,#0x00
    ld  (_key_raw_ir+1),a   ;blank

wait_for_start_bit:
    ldh a,(#0x56)
    bit 4,a     ;test pin 4 of GBC serial port
    jp  z,wait_for_start_bit

wait_for_start_bit_high:
    ldh a,(#0x56)
    bit 4,a
    jp  nz,wait_for_start_bit_high
    
wait_for_first_bit_low:
    ldh a,(#0x56)
    bit 4,a
    jp  z,wait_for_first_bit_low

    ld  a,#0x80
    ldh (#0x02),a       ;start serial xfer (external clock)

wait_for_key_byte:

    ldh a,(#0x02)
    bit 7,a
    jp  nz,wait_for_key_byte

    ldh a,(#0x01)
    call    change_msb_order
    ld  a,b
    ld  (_key_raw_ir),a

wait_for_stop_bit_clock_high:
    ldh a,(#0x56)
    bit 4,a
    jp  nz,wait_for_stop_bit_clock_high

    ld  a,(_key_raw_ir) ;check for extended key
    cp  #0xE0
    ei
    ret nz              ;return from here if NOT extended key

wait_for_start_bit2:
    di
    ldh a,(#0x56)
    bit 4,a     ;test pin 4 of GBC serial port
    jp  z,wait_for_start_bit2

wait_for_start_bit_high2:
    ldh a,(#0x56)
    bit 4,a
    jp  nz,wait_for_start_bit_high2
    
wait_for_first_bit_low2:
    ldh a,(#0x56)
    bit 4,a
    jp  z,wait_for_first_bit_low2

    ld  a,#0x80
    ldh (#0x02),a       ;start serial xfer (external clock)

wait_for_key_byte2:
    ldh a,(#0x02)
    bit 7,a
    jp  nz,wait_for_key_byte2

    ldh a,(#0x01)
    call    change_msb_order
    ld  a,b
    ld  (_key_raw_ir+1),a

wait_for_stop_bit_clock_high2:
    ldh a,(#0x56)
    bit 4,a
    jp  nz,wait_for_stop_bit_clock_high2
    ei
    ret

change_msb_order:       ;enter in a
    ld  b,#0x80         ;leave in b
do_next_bit:
    rlca
    rr  b
    ret c
    jp  nz,do_next_bit

;End of kb.s
