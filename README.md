# PS2 keyboard handler for Gameboy color
Connection for between IBM-PC(comatible) Keyboard and GB. Original is coded for RGBASM by Mr. Ken Kaarvik. Mr.K.I of TeamKNOx have re-designed and added several feature as Japanese Keyboard handling in GBDK which is used in TeamKNOx.(common language) 

## How it works
<table>
<tr>
<td><img src="./pics/gb-key.jpg" width="50%"></td>
</tr>
</table>
Keyboard of IBM-PC(and comaptible) is handling key board data with clock. Sync with clock means to avoid data lost... However, Clock line is needed.(Extra cost will be occured.) OK, Let's speask How it works...most important point is that clock line and input line are both connected to one terminal. That means super visoing input line with clock timing. In fact, Behavir of keyboard is more complecated. Diagnosis has been executed in POST(Power On Self Test). Constant Key code will be generated at make & break, keyboard driver is handling key sequense and so on.

## Connector cable
<table>
<tr>
<td><img src="./pics/gb-key_cbl.jpg" width="50%"></td>
<td><img src="./pics/gb-key_cnt.jpg" width="50%"></td>
<td><img src="./pics/gb-key_gb.jpg" width="50%"></td>
</tr>
</table>
You have to make connector cable for GB-KEY. It is necessary to purchase calbe which is all line connected. Genuien Nintendo cable does not handle this issue. Therefore, we have to use GameTech. Mail order is available. You can make two connector cable from one package. Also you have to prepare MiniDin6P(Female) connector. I bought in Akihabara. 

## Examples
<table>
<tr>
<td><img src="./pics/gb-key_cmp.jpg" width="50%"></td>
<td><img src="./pics/10key.jpg" width="50%"></td>
</tr>
</table>
OK !! Connection is Good. Something else possible to connect to 10key and barcode reader. Also planning to make special application for them. 

## GB-KEY Application
GB-KEY is basic driver. GB-PDA and other software will be (re)designed for GB-KEY.


# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
